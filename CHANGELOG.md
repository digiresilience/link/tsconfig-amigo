# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.4](https://gitlab.com/digiresilience.org/link/tsconfig-amigo/compare/0.1.3...0.1.4) (2021-05-27)

### [0.1.3](https://gitlab.com/digiresilience.org/link/tsconfig-amigo/compare/0.1.2...0.1.3) (2021-05-03)


### Features

* bump target to es2020 ([8546663](https://gitlab.com/digiresilience.org/link/tsconfig-amigo/commit/854666318e42efba15d69fbab87cf97dc889c559))

### [0.1.2](https://gitlab.com/digiresilience.org/link/tsconfig-amigo/compare/0.1.1...0.1.2) (2020-11-16)


### Bug Fixes

* remove type roots, as this causes problems with jest ([de014ec](https://gitlab.com/digiresilience.org/link/tsconfig-amigo/commit/de014ecb74c81e21017cdbd327c628469af8a427))

### [0.1.1](https://gitlab.com/digiresilience.org/link/tsconfig-amigo/compare/0.1.0...0.1.1) (2020-10-09)


### Bug Fixes

* remove project specific opts ([629efc7](https://gitlab.com/digiresilience.org/link/tsconfig-amigo/commit/629efc789e0b5608e3583c8c2f92a17c61de6dbc))

## 0.1.0 (2020-10-09)
